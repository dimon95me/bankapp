package retrofit;

import retrofit.model.user.UserJsonShell;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface BankingServerApi {

    @GET("login_response.php")
    Call<UserJsonShell> login(@Query("login_field") String login, @Query("password") String password);
}
