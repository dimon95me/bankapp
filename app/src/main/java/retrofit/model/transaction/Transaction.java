package retrofit.model.transaction;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Transaction {

    @SerializedName("id_transaction")
    private String idTransaction;

    @SerializedName("note")
    @Expose
    private String note;

    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("id_loan_recipients")
    @Expose
    private String idLoanRecipients;
    @SerializedName("id_loan_senders")
    @Expose
    private String idLoanSenders;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("recipient_name")
    @Expose
    private String recipientName;
    @SerializedName("recipient_surname")
    @Expose
    private String recipientSurname;

    public String getNote() {
        return note;
    }

    public String getIdTransaction() {
        return idTransaction;
    }

    public void setIdTransaction(String idTransaction) {
        this.idTransaction = idTransaction;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getIdLoanRecipients() {
        return idLoanRecipients;
    }

    public void setIdLoanRecipients(String idLoanRecipients) {
        this.idLoanRecipients = idLoanRecipients;
    }

    public String getIdLoanSenders() {
        return idLoanSenders;
    }

    public void setIdLoanSenders(String idLoanSenders) {
        this.idLoanSenders = idLoanSenders;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getRecipientName() {
        return recipientName;
    }

    public void setRecipientName(String recipientName) {
        this.recipientName = recipientName;
    }

    public String getRecipientSurname() {
        return recipientSurname;
    }

    public void setRecipientSurname(String recipientSurname) {
        this.recipientSurname = recipientSurname;
    }

}