package interfaces;

public interface ShowDialogInterface {
    void showProgress();
    void hideProgress();
}
