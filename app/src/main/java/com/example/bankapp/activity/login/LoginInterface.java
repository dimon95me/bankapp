package com.example.bankapp.activity.login;

import interfaces.ShowDialogInterface;
import retrofit.model.user.User;

public interface LoginInterface extends ShowDialogInterface {
    void onSuccess(User user);

    void onFailed(String error);

    Boolean nfcVerify(String serverPass);

    void setMessageFromNfcTag(String serverPass);
}
