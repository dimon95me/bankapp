package com.example.bankapp.activity.main;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bankapp.R;
import com.example.bankapp.activity.main.main_recycler_view.MainAllLoansAdapter;

import java.util.List;

import retrofit.model.loan.Loan;
import retrofit.model.user.User;

public class MainActivity extends AppCompatActivity implements MainInterface{
    private static final String TAG = "MainActivity";

    private TextView usernameTextView, surnameTextView;
    private RecyclerView recyclerView;

    private String localServerPass;
    private ProgressDialog progressDialog;
    private MainPresenter mainPresenter;
    private MainAllLoansAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        usernameTextView = findViewById(R.id.main_username_textview);
        surnameTextView = findViewById(R.id.main_surname_textview);

        recyclerView = findViewById(R.id.main_loan_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        mainPresenter = new MainPresenter(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please, wait...");

        Intent intent = this.getIntent();
        localServerPass = intent.getStringExtra("server_password");
        Log.d(TAG, "onCreate: serverLocalPass "+ localServerPass);


        mainPresenter.getUserByServerPass(localServerPass);
        mainPresenter.getLoansByServerPass(localServerPass);

    }

    @Override
    public void onSuccessUser(User user) {
        usernameTextView.setText("Hello, dear " + user.getName());
        surnameTextView.setText(user.getSurname());
    }

    @Override
    public void onFailedUser(String error) {
        Log.d(TAG, "onFailedUser: "+error);
        Toast.makeText(this, "onFailedUser", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSuccessAllLoans(List<Loan> loans) {
        adapter = new MainAllLoansAdapter(loans, this, localServerPass);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onFailedAllLoans(String error) {
        Toast.makeText(this, "onFAiledAllLoans", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgress() {
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }
}
