package com.example.bankapp.activity.loan;

import java.util.List;

import interfaces.ShowDialogInterface;
import retrofit.model.loan.Loan;
import retrofit.model.transaction.Transaction;

public interface LoanInterface extends ShowDialogInterface {

    void onSuccessLoan(Loan loan);

    void onFailedLoan(String messageError);

    void onSuccessTransactions(List<Transaction> transactionList);

    void onFailedTransactions(String messageError);
}
