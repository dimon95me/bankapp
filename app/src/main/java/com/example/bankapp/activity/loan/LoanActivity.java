package com.example.bankapp.activity.loan;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bankapp.R;
import com.example.bankapp.activity.loan.loan_recycler_view.LoanTransactionsAdapter;

import java.util.List;

import retrofit.model.loan.Loan;
import retrofit.model.transaction.Transaction;

public class LoanActivity extends AppCompatActivity implements LoanInterface {
    private static final String TAG = "LoanActivity";

    private int idLoan;
    private String localServerPass;

    private TextView nameTextView, amountTextView, dateTextView, durationTextView;
    private ProgressDialog progressDialog;

    private RecyclerView recyclerView;
    private LoanTransactionsAdapter adapter;

    private LoanPresenter presenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loan);

        presenter = new LoanPresenter(this);

        initializeViews();

        Intent gettedIntent = getIntent();
        idLoan = gettedIntent.getIntExtra("id", 0);
        localServerPass = gettedIntent.getStringExtra("pass");

        presenter.getLoanByIdAndServerPass(idLoan, localServerPass);
        presenter.getTransactionListByIdLoan(String.valueOf(idLoan));
    }

    private void initializeViews() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please, wait...");

        nameTextView = findViewById(R.id.loan_activity_name);
        amountTextView = findViewById(R.id.loan_activity_amount);
        dateTextView = findViewById(R.id.loan_activity_creating_date);
        durationTextView = findViewById(R.id.loan_activity_duration);

        recyclerView = findViewById(R.id.activity_loan_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void onSuccessLoan(Loan loan) {
        nameTextView.setText("Name: " + loan.getName());
        durationTextView.setText("Duration: " + loan.getDuration());
        dateTextView.setText("Date: " + loan.getCreatingDate());
        amountTextView.setText("Amount: " + loan.getAmount());
    }

    @Override
    public void onFailedLoan(String messageError) {
        Log.d(TAG, "onFailedLoan: " + messageError);
    }

    @Override
    public void onSuccessTransactions(List<Transaction> transactionList) {
        adapter = new LoanTransactionsAdapter(transactionList, this, String.valueOf(idLoan));
        Log.d(TAG, "onSuccessTransactions: "+transactionList);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onFailedTransactions(String messageError) {
        Toast.makeText(this, "Transactions are failed", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgress() {
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }
}
