package com.example.bankapp.activity.loan;

import android.util.Log;

import retrofit.ApiClient;
import retrofit.model.loan.Loan;
import retrofit.model.transaction.Transactions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoanPresenter {
    private static final String TAG = "LoanPresenter";

    private LoanInterface view;
    private LoanApi api;

    public LoanPresenter(LoanActivity view) {
        this.view = view;
    }

    void getLoanByIdAndServerPass(int idLoan, String serverPass) {
        view.showProgress();

        if (api==null) api = ApiClient.getApiClient().create(LoanApi.class);
        Call<Loan> callLoan = api.getAllLoansByServerPass(String.valueOf(idLoan), serverPass);

        callLoan.enqueue(new Callback<Loan>() {
            @Override
            public void onResponse(Call<Loan> call, Response<Loan> response) {
                view.onSuccessLoan(response.body());
                view.hideProgress();
            }

            @Override
            public void onFailure(Call<Loan> call, Throwable t) {
                view.onFailedLoan(t.getLocalizedMessage());
                Log.e(TAG, "onFailure: loan from server ",t );
                view.hideProgress();
            }
        });
    }

    void getTransactionListByIdLoan(String idLoan) {
        view.showProgress();

        if (api==null) api = ApiClient.getApiClient().create(LoanApi.class);
        Call<Transactions> transactionsCall = api.getTransactionsByIdLoan(idLoan);

        transactionsCall.enqueue(new Callback<Transactions>() {
            @Override
            public void onResponse(Call<Transactions> call, Response<Transactions> response) {
                view.onSuccessTransactions(response.body().getTransactions());
                view.hideProgress();
            }

            @Override
            public void onFailure(Call<Transactions> call, Throwable t) {
                view.onFailedTransactions(t.getLocalizedMessage());
                view.hideProgress();
            }
        });

    }
}
