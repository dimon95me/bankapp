package com.example.bankapp.activity.main.main_recycler_view;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bankapp.R;
import com.example.bankapp.activity.loan.LoanActivity;

import java.util.ArrayList;
import java.util.List;

import retrofit.model.loan.Loan;

public class MainAllLoansAdapter extends RecyclerView.Adapter<MainAllLoansAdapter.ViewHolder> {
    private List<Loan> loans = new ArrayList<>();
    Context context;

    String localServerPass;

    public MainAllLoansAdapter(List<Loan> loans, Context context, String localServerPass) {
        this.loans = loans;
        this.context = context;
        this.localServerPass = localServerPass;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.main_loan_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.typeText.setText(loans.get(i).getName());
        viewHolder.countText.setText(loans.get(i).getAmount());
        viewHolder.dateText.setText(loans.get(i).getCreatingDate());
        viewHolder.idLoan = Integer.parseInt(loans.get(i).getIdLoan());
    }

    @Override
    public int getItemCount() {
        return loans.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView countText, typeText, dateText;
        private int idLoan;

        private LinearLayout layout;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            countText = itemView.findViewById(R.id.main_loan_item_count);
            dateText = itemView.findViewById(R.id.main_loan_item_date);
            typeText = itemView.findViewById(R.id.main_loan_item_type);

            layout = itemView.findViewById(R.id.main_item_linear_layout);
            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Toast.makeText(itemView.getContext(), "fsdf", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(itemView.getContext(), LoanActivity.class);
                    intent.putExtra("id", idLoan);
                    intent.putExtra("pass", localServerPass);
                    context.startActivity(intent);
                }

            });
        }


    }
}
