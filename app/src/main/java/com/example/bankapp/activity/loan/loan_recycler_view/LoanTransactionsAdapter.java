package com.example.bankapp.activity.loan.loan_recycler_view;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.bankapp.R;

import java.util.List;

import retrofit.model.transaction.Transaction;

public class LoanTransactionsAdapter extends RecyclerView.Adapter<LoanTransactionsAdapter.ViewHolder> {
    private List<Transaction> transactions;
    private Context context;
    private String mainLoanId;

    public LoanTransactionsAdapter(List<Transaction> transactions, Context context, String mainLoanId) {
        this.transactions = transactions;
        this.context = context;
        this.mainLoanId = mainLoanId;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.loan_transactions_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.noteTextView.setText(transactions.get(i).getNote());
        viewHolder.dateTextView.setText(transactions.get(i).getDate());
        viewHolder.idLoanRecipientTextView.setText(transactions.get(i).getIdLoanRecipients());
        viewHolder.idLoanSenderTextView.setText(transactions.get(i).getIdLoanSenders());

        if (mainLoanId.equals(transactions.get(i).getIdLoanSenders())){ viewHolder.amountTextView.setText(transactions.get(i).getAmount());}
        else {
            Log.d("mainIdLoan", mainLoanId);
            viewHolder.amountTextView.setText("-"+transactions.get(i).getAmount());
            viewHolder.amountTextView.setTextColor(Color.RED);
        }

        viewHolder.recipientNameTextView.setText(transactions.get(i).getRecipientName());
        viewHolder.recipientSurnameTextView.setText(transactions.get(i).getRecipientSurname());
        viewHolder.idTransaction = transactions.get(i).getIdTransaction();
    }

    @Override
    public int getItemCount() {
        return transactions.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private String idTransaction;

        private TextView noteTextView, dateTextView, idLoanRecipientTextView, idLoanSenderTextView, amountTextView, recipientNameTextView, recipientSurnameTextView;
        private LinearLayout linearLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            noteTextView = itemView.findViewById(R.id.loan_transactions_item_note);
            dateTextView = itemView.findViewById(R.id.loan_transactions_item_date);
            idLoanRecipientTextView = itemView.findViewById(R.id.loan_transactions_item_id_loan_recipient);
            idLoanSenderTextView = itemView.findViewById(R.id.loan_transactions_item_id_loan_sender);
            amountTextView = itemView.findViewById(R.id.loan_transactions_item_amount);
            recipientNameTextView = itemView.findViewById(R.id.loan_transactions_item_name);
            recipientSurnameTextView = itemView.findViewById(R.id.loan_transactions_item_surname);

            linearLayout = itemView.findViewById(R.id.loan_transactions_item_linear_layout);
            linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
        }
    }
}
