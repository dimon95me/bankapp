package com.example.bankapp.activity.main;

import retrofit.model.loan.AllLoans;
import retrofit.model.user.UserJsonShell;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface MainApi {
    @GET("get_user_by_server_pass.php")
    Call<UserJsonShell> getUserByServerPass(@Query("server_pass") String serverPass);

    @GET("get_loans_by_server_pass.php")
    Call<AllLoans> getAllLoansByServerPass(@Query("server_pass") String serverPass);
}
