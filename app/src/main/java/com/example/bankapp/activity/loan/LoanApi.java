package com.example.bankapp.activity.loan;

import retrofit.model.loan.Loan;
import retrofit.model.transaction.Transactions;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

interface LoanApi {

    @GET("get_loan_by_id_loan_and_server_pass.php")
    Call<Loan> getAllLoansByServerPass(@Query("id_loan") String idLoan, @Query("server_pass") String serverPass);

    @GET("get_transactions_by_id_loan.php")
    Call<Transactions> getTransactionsByIdLoan(@Query("id_loan") String idLoan);
}
