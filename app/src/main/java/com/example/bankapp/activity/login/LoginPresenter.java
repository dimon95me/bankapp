package com.example.bankapp.activity.login;

import android.util.Log;

import retrofit.ApiClient;
import retrofit.BankingServerApi;
import retrofit.model.user.User;
import retrofit.model.user.UserJsonShell;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginPresenter {

    private static final String TAG = "LoginPresenter";

    private LoginInterface view;

    public LoginPresenter(LoginActivity view) {
        this.view = (LoginInterface) view;
    }

    void signIn(String login, String password) {
        view.showProgress();
//        BankingServerApi serverApi = NetworkService.getInstance().getJSONApi();
        BankingServerApi serverApi = ApiClient.getApiClient().create(BankingServerApi.class);
        Call<UserJsonShell> call = serverApi.login(login, password);

        call.enqueue(new Callback<UserJsonShell>() {


            @Override
            public void onResponse(Call<UserJsonShell> call, Response<UserJsonShell> response) {
                if (response.isSuccessful() && response.body()!=null) {
                    Boolean success = response.body().getSuccess();
                    if (success) {

//                        if (response.body().getUser().)
                        User user = response.body().getUser();
                        Log.d("user", user.getName()+" \n"+
                                user.getSurname()+" \n"+
                                user.getAddress()+" \n"+
                                user.getEmail()+" \n"+
                                user.getPhone()+" \n"+
                                user.getServerPass()+" \n"+
                                user.getNfcIsOn()+" \n"+
                                user.getSecretQuestion()+" \n"+
                                user.getSecretAnswer()+" \n");//Output data of user to console

                        if (user.getNfcIsOn().equalsIgnoreCase("1")) {
                            Log.d("NFCis1", "User has enable nfc security");
//                            if (view.nfcVerify() == null) {
//
//                                return;}
                            if (!view.nfcVerify(user.getServerPass())){
                                Log.d(TAG, "onResponse: serverpass and tagpass are different. Maybe tagPass is null");
                                return;
                            }
                        }

                        view.onSuccess(response.body().getUser());
                    } else {
                        view.onFailed(response.body().getMessage());
                    }
                    view.hideProgress();
                }
            }

            @Override
            public void onFailure(Call<UserJsonShell> call, Throwable t) {
                view.onFailed(t.getLocalizedMessage());
                view.hideProgress();
            }
        });
    }

    void verifyTag() {

    }
}
