package com.example.bankapp.activity.main;

import android.util.Log;

import java.util.List;

import retrofit.ApiClient;
import retrofit.model.loan.AllLoans;
import retrofit.model.loan.Loan;
import retrofit.model.user.User;
import retrofit.model.user.UserJsonShell;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainPresenter {
    private static final String TAG = "MainPresenter";

    private MainInterface view;
    private String idKlient;
    private MainApi api;

    public MainPresenter(MainActivity view) {
        this.view = (MainInterface) view;
    }

    void getUserByServerPass(String serverPass) {
        view.showProgress();

        if (api == null) api = ApiClient.getApiClient().create(MainApi.class);
        Call<UserJsonShell> callUser = api.getUserByServerPass(serverPass);

        callUser.enqueue(new Callback<UserJsonShell>() {
            @Override
            public void onResponse(Call<UserJsonShell> call, Response<UserJsonShell> response) {
                if (response.isSuccessful() && response.body() != null) {
                    Boolean success = response.body().getSuccess();
                    if (success) {

                        //                        if (response.body().getUser().)
                        User user = response.body().getUser();
                        Log.d("user", user.getName() + " \n" +
                                user.getSurname() + " \n" +
                                user.getAddress() + " \n" +
                                user.getEmail() + " \n" +
                                user.getPhone() + " \n" +
                                user.getServerPass() + " \n" +
                                user.getNfcIsOn() + " \n" +
                                user.getSecretQuestion() + " \n" +
                                user.getSecretAnswer() + " \n");//Output data of user to console

                        idKlient = response.body().getUser().getIdKlent();
                        Log.d(TAG, "onResponse: idKlient: " + idKlient);
                        view.onSuccessUser(response.body().getUser());
                    }
                }
                view.hideProgress();
                Log.d(TAG, "onResponse: mainSuccess");
            }

            @Override
            public void onFailure(Call<UserJsonShell> call, Throwable t) {
                Log.d(TAG, "onFailure: mainFailure");
                view.onFailedUser(t.getLocalizedMessage());
                view.hideProgress();
            }
        });
    }

    public void getLoansByServerPass(String serverPass) {
        view.showProgress();

        if (api == null) api = ApiClient.getApiClient().create(MainApi.class);
        Call<AllLoans> callLoans = api.getAllLoansByServerPass(serverPass);

        callLoans.enqueue(new Callback<AllLoans>() {
            @Override
            public void onResponse(Call<AllLoans> call, Response<AllLoans> response) {

                if (response.isSuccessful() && response.body() != null) {
                    Boolean success = response.body().getSuccess();
                    if (success) {

                        List<Loan> loans = response.body().getLoan();
                        Log.d(TAG, "onResponse: listLoan"+ loans.toString());
                        view.onSuccessAllLoans(loans);

                    }
                }

                view.hideProgress();
                Log.d(TAG, "onResponse: mainAllLoansSuccess");
            }

            @Override
            public void onFailure(Call<AllLoans> call, Throwable t) {
                Log.e(TAG, "onFailure: mainLoanFailure", t);
                view.onFailedAllLoans(t.getLocalizedMessage());
                view.hideProgress();
            }
        });

    }
}
