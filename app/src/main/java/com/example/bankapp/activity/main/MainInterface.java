package com.example.bankapp.activity.main;

import java.util.List;

import interfaces.ShowDialogInterface;
import retrofit.model.loan.Loan;
import retrofit.model.user.User;

public interface MainInterface extends ShowDialogInterface {

    void onSuccessUser(User user);
    void onFailedUser(String error);

    void onSuccessAllLoans(List<Loan> loans);
    void onFailedAllLoans(String error);

}
